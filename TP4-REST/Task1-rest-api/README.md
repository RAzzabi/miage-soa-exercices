# Spring Boot Rest API Example

## Introduction

Spring Boot complements Spring REST support by providing default dependencies/converters out of the box. Writing RESTful services in Spring Boot is no-different than Spring MVC. If you are a REST Client [Rest Consumer], Spring Boot provides [RestTemplateBuilder](http://docs.spring.io/spring-boot/docs/current/api/org/springframework/boot/web/client/RestTemplateBuilder.html) that can be used to customize the RestTemplate before calling the REST endpoints.

__JSON REST service__

* Any Spring __@RestController__ in a Spring Boot application will render JSON response by default as long as Jackson2 [jackson-databind] is on the classpath.

__XML REST service__

* For enabling XML representations, Jackson XML extension (jackson-dataformat-xml) must be present on the classpath. Add the following dependency to your project:
```
<dependency>
    <groupId>com.fasterxml.jackson.dataformat</groupId>
    <artifactId>jackson-dataformat-xml</artifactId>
</dependency>
```
* __Note__ : In order to get XML response instead of JSON, consumer is expected to send appropriate ‘Accept’ header with value ‘text/xml’ or ‘application/xml’.

__REST__

* REST stands for Representational State Transfer.It’s an is an architectural style which can be used to design web services, that can be consumed from a variety of clients. The core idea is that, rather than using complex mechanisms such as CORBA, RPC or SOAP to connect between machines, simple HTTP is used to make calls among them.

In Rest based design, resources are being manipulated using a common set of verbs __CRUD__.

- To Create a resource : HTTP POST should be used
- To Retrieve a resource : HTTP GET should be used
- To Update a resource : HTTP PUT should be used
- To Delete a resource : HTTP DELETE should be used

# Practice
## 1- Download Rest API example
The REST Api example is located at https://framagit.org/RAzzabi/miage-soa-exercices/tree/master/TP4-REST/Task1-rest-api

Source code can be download as __Zip__ file __or__ by using __Git__

a) Download via __Git__
- Create a new folder your home directory named __miage-gitlab__
- Once created, __Shift + rightClick__ on __miage-gilab__ folder and choose __open commande window here__
- Fetch the example API source code from the Git repository by running
```
git clone https://framagit.org/RAzzabi/miage-soa-exercices.git
```
- A new folder named miage-soa-exercices will be created
Excercie is located at TP4-REST/Task1-rest-api

b) Download as Zip file
- Download directly the zip file by clicking on the download button located at the top right off the web page

## 2- Start the service provider
- Open Netbeans IDE, locate the downloaded folder and add it as a Netbeans new project
- Clean and build the project
- Run the project
- Test the service by accesing http://localhost:8081/SpringBootRestApi/api

## 3- Exploring the REST API

a) Open the __Source Packages__, and explore the differentes classes.

b) Find the __Rest_Controller__, read the source, and try to comment each request   

c) Complete the following statement :
The REST API example does:
- __GET__ request to /api/user/ --> returns a list of users
- __GET__ request to /api/user/1 --> ...............................
- __POST__ request to /api/user/ --> ...............................
- __PUT__ request to /api/user/3 --> ...............................
- __DELETE__ request to /api/user/4 --> ...............................
- __DELETE__ request to /api/user/ --> ...............................

## 4- Testing the API

```
NOTE : Take a screenshot for each step
```

a) Install Postman :
- as a Chrome extension (launch Chrome, then open menu, More tools, Extension, Add extension, search for Postman)
- OR as a native app, see here https://www.getpostman.com/apps

b) Retrieve all users

- Open POSTMAN tool, select request type [__GET__ for this usecase], specify the uri http://localhost:8080/SpringBootRestApi/api/user/ and Send., should retrieve all users.
- Step 1 : get list users on Json format (default format)
- Step 2 : get list users on XML format : Hint !! you should configure the request header to accept xml response


c) Find user by id
- Use GET http://localhost:8080/SpringBootRestApi/api/user/${user_id} to retrive information about the user __sam__

d) Retrieve an unknown user
- Use GET, specify the wrong id of the user, and send.
- Explain what happened

e) Create a User
- Use POST, specify the content in body, select content-type as ‘application/json’
```
{
  "id": value,
  "name": value,
  "age": value,
  "salary": value
}
```

f) Create a User with an existing user-name
- Use POST, specify the content in body with name of an existing user,Send, should get a 409/conflict.

g) Update an existing user
- Use PUT, specify the content in body and type as ‘application/json’.

h) Delete an existing user
- Use DELETE, specify the id in url, and send.
- User should be deleted from server.

i) Verify the results
- Use GET to retrieve all users, and send, should not get any user in response.
