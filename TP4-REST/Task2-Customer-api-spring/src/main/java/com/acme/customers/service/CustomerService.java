/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acme.customers.service;

import com.acme.customers.model.Customer;
import java.util.List;

/**
 *
 * @author RA234587
 */

public interface CustomerService {
    List<Customer> getAllCustomers();
    //Customer findById(long id);
    //Customer findByName(String name);
    //boolean isCustomerExist(Customer customer);
    //void saveCustomer(Customer customer);
    //void updateCustomer(Customer customer);
    //void deleteCustomerById(long id);
    //void deleteAllCustomers();

}
