# Working session : developping a REST web service with Spring


1/ Make the first test pass (helloworld test), by implementing the controller (see here if needed => https://spring.io/guides/gs/rest-service/)

- have a look at the test, how to read it ? look to the test Packages / com.acme.customers.server.api
**CustomerApiUnitTest.Java**
```
@Test
public void helloWorldTest() throws Exception {
    System.out.println("Testing helloWorldTest API-----------");
    String body = this.restTemplate.getForObject("/test/hello", String.class);
    assertEquals("Hello", body);
}
```
- implement the controller, by using Spring annotations (@RestController, @RequestMapping)
look to the source Packages / com.acme.customers.server.controller
**HelloController.java**
```
//add rest controller annotation
public class HelloController {

    //Add request mapping
    //add response status
    public String helloWord() {
        return "Hello";
    }

}
```

2/ Make the second test pass, getCustomers.
Tips : Return a collection of "Customers"
Once the test is OK, run your project, test it with postman
- how does the json is generated ?

3/ Make the "getCustomerById" test pass
Use Spring annotation @PathVariable
Run & Test with Postman

4/ Make the postCustomerTest pass
- Use Spring annotation @RequestBody & @ResponseStatus
- how does the json is translated to Java ?
- which status code should be returned ?
Run & Test with Postman

5/ Make All Test pass
- Implement the business logic of the application
- Create a "CustomerService.java" interface, with methods allowing creation & search of customers
- Create an implementation for this interface
- Update your existing controller
- Rq : the tests should be broken - use @Ignore to desactivate them
- implement a PUT operation (update an existing customer)
- implement a DELETE operation

6/ implement the 3 possible approaches for exception handling
- use one of the 3 approaches here : https://spring.io/blog/2013/11/01/exception-handling-in-spring-mvc
Run & test with Postman

7/ implement XML binding : depending on the Accept header provided in the request, return a json response, or a XML response

8/ Change the name of the fields generated in the response - without impacting the code
- firstName becomes givenName
- lastName becomes familyName
- fix the order to have : id, then familyName, then firstName
- do this implementation for both json and xml
Tips : for json,  add jackson annotations in the Customer.java class. For xml, add JAXB annotation in the DTO class
