## **Task 1**

Complete the following JSON file

```

  "store":
    "stock":

        "name": "Fruits and Vegetables",
        "product":

            "ref": "apple",
            "prix": "2€",
            "code": "43",
            "available": "true"
          ,

            "ref": "pear",
            "prix": "1€",
            "code": "89",
            "available": "false"
          ,

            "ref": "carrot",
            "prix": "1€",
            "code": "68",
            "available": "true"


      ,

        "name": "meat",
        "product":

            "ref": "chicken",
            "prix": "4€",
            "code": "28",
            "available": "false"
          ,

            "ref": "beef",
            "prix": "3€",
            "code": "12",
            "available": "true"






```

## **Task 2** : Creating a JSON
Create a JSON file representing a list of messages. A message has a subject, a recipient, a sender, a date, and a content. It can belong to one or more categories. Categories can be added, deleted or updated. A message content can be normal or important.


## **Task 3** : Parse JSON in Java

Below is a simple example from Wikipedia that shows JSON representation of an object that describes a person. The object has string values for first name and last name, a number value for age, an object value representing the person’s address, and an array value of phone number objects

```
{
    "firstName": "John",
    "lastName": "Smith",
    "age": 25,
    "address": {
        "streetAddress": "21 2nd Street",
        "city": "New York",
        "state": "NY",
        "postalCode": 10021
    },
    "phoneNumbers": [
        {
            "type": "home",
            "number": "212 555-1234"
        },
        {
            "type": "fax",
            "number": "646 555-4567"
        }
    ]
}
```

**JSON Processing in Java** : The Java API for JSON Processing [JSON.simple](https://code.google.com/archive/p/json-simple/) is a simple Java library that allow parse, generate, transform, and query JSON.

**Json-Simple API** : It provides object models for JSON object and array structures. These JSON structures are represented as object models using types JSONObject and JSONArray. JSONObject provides a Map view to access the unordered collection of zero or more name/value pairs from the model. Similarly, JSONArray provides a List view to access the ordered sequence of zero or more values from the model.

**Java class to write a JSON file**
```
// Java program for write JSON to a file
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.LinkedHashMap;
import java.util.Map;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
public class JSONWriteExample
{
	public static void main(String[] args) throws FileNotFoundException
	{
		// creating JSONObject
		JSONObject jo = new JSONObject();

		// putting data to JSONObject
		jo.put("firstName", "John");
		jo.put("lastName", "Smith");
		jo.put("age", 25);

		// for address data, first create LinkedHashMap
		Map m = new LinkedHashMap(4);
		m.put("streetAddress", "21 2nd Street");
		m.put("city", "New York");
		m.put("state", "NY");
		m.put("postalCode", 10021);

		// putting address to JSONObject
		jo.put("address", m);

		// for phone numbers, first create JSONArray
		JSONArray ja = new JSONArray();

		m = new LinkedHashMap(2);
		m.put("type", "home");
		m.put("number", "212 555-1234");

		// adding map to list
		ja.add(m);

		m = new LinkedHashMap(2);
		m.put("type", "fax");
		m.put("number", "212 555-1234");

		// adding map to list
		ja.add(m);

		// putting phoneNumbers to JSONObject
		jo.put("phoneNumbers", ja);

		// writing JSON to file:"JSONExample.json" in cwd
		PrintWriter pw = new PrintWriter("JSONExample.json");
		pw.write(jo.toJSONString());

		pw.flush();
		pw.close();
	}
}
```

### Questions:
- 1/ Configure and run the **JSONWriteExample** project
- 2/ Write a java program to read JSON from a file. Complete and run the Java example bellow :

```
// Java program to read JSON from a file
import java.io.FileReader;
import java.util.Iterator;
import java.util.Map;
import org.json.simple.parser.*;
public class JSONReadExample
{
	public static void main(String[] args) throws Exception
	{
		// parsing file "JSONExample.json"
		Object obj = new JSONParser().parse(new FileReader("JSONExample.json")); 		
		// typecasting obj to JSONObject

		// getting firstName and lastName

		// getting age

		// getting address

		// iterating address Map  

		// getting phoneNumbers (JSONArray)

		// iterating phoneNumbers

	}
}
```

  The **JSONReadExample** class must read data from the  **JSONExample.json** file generated on (1) and produce the output bellow:
  ```
  John
  Smith
  25
  streetAddress : 21 2nd Street
  postalCode : 10021
  state : NY
  city : New York
  number : 212 555-1234
  type : home
  number : 212 555-1234
  type : fax
```  
