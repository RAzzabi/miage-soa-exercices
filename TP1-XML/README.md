# **Exercice 1** : XML


## **Task 1**: Hello XML

For each of the following XML files, determine if it is well written and correct the errors if exist

- **Document 1**: simple1.xml
- **Document 2**: simple2.xml
- **Document 3**: simple3.xml
- **Document 4**: simple4.xml
- **Document 5**: simple5.xml, file tokyo.xml and newyork.xml

## **Task 2** : Creating an XML document

Below an extract from the **biblio.bib** file . It's a BiBTeX file, a system for listing of bibliographic references.
```
@InProceedings{DalzilioS:learnbs,
  author = "Dal Zilio, Silvano
            and Bernard, Thierry M.",
  title = "Learning Binary Shapes as Compression and its
	   Cellular Implementation",
  booktitle = "ACCV '95 -- 2nd Asian Conference on Computer
               Vision",
  year = 1995,
  volume = 2,
  pages = "616--620",
  month = dec,
  url = "http://www.cmi.univ-mrs.fr/~dalzilio/Papers/accv95.ps",
  abstract = "We present a methodology to learn how to
   	 recognize binary shapes, based on the principle that
   	 recognition may be understood as a process of
   	 information compression. Our approach, directed
   	 towards adaptive target tracking applications, is
   	 intended to be well suited to fine-grained parallel
   	 architectures, such as cellular automata machines
   	 that can be integrated in artificial retinas. This
   	 methodology, fruitfully explained within the frame
   	 of mathematical morphology, is then particularized
   	 in the perspective of its actual implementation."

```

This entry represents a citation of a scientifc paper published on a conference. it can be represented as an XML document as follow:
```
<?xml version='1.0'?>

<bibtex-file>
 <inproceedings>
   <key>DalzilioS:learnbs</key>
   <author>
     <name>Dal Zilio, Silvano</name>
     <name>Bernard, Thierry M.</name>
   </author>
   <title>Learning Binary Shapes as Compression and its Cellular
   Implementation</title>
   <booktitle>
     <short>ACCV '95</short>
     <long>2nd Asian Conference on Computer Vision</long>
   </booktitle>
   <year>1995</year>
   <volume>2</volume>
   <pages first="616" last="620"/>
   <month mtype="short">dec</month>
   <url ftype="ps"
	href="http://www.cmi.univ-mrs.fr/~dalzilio/Papers/accv95.ps"/>
   <abstract>
     We present a methodology to learn how to recognize binary shapes,
     based on the principle that recognition may be understood as a
     process of information compression. Our approach, directed
     towards adaptive target tracking applications, is intended to be
     well suited to fine-grained parallel architectures, such as
     cellular automata machines that can be integrated in artificial
     retinas. This methodology, fruitfully explained within the frame
     of mathematical morphology, is then particularized in the
     perspective of its actual implementation.
   </abstract>
 </inproceedings>
</bibtex-file>
```

### Questions :
- 1/ Translate the entrie bib file **biblio.bib** as an XML document.
- 2/ Explain your choices.


## Task3 : Parsing XML

In this section, we will use DOM or SAX to parse an input XML file and extract information from it.
- 1/ Read the Java tutorials about DOM at https://docs.oracle.com/javase/tutorial/jaxp/dom/readingXML.html and about SAX at https://docs.oracle.com/javase/tutorial/jaxp/sax/parsing.html.

- 2/ Parse the **staff.xml** file using the **ReadXMLFileDOM.java** and **ReadXMLFileSAX.java**  

- 3/ Open the OpenStreetMap website at http://www.openstreetmap.org/, and select a
zone in the world (by zooming in and out). Then, export it using the Export button on the webpage.You will obtain a file called map in the OSM XML format. Take the time to familiarize yourself with the OSM XML format by looking over the file and consulting http://wiki.openstreetmap.org/wiki/OSM_XML.

- 4/ Use a Java DOM or SAX parser to load and parse the map XML file. Use the program
to identify and print all the names appearing in the map.
