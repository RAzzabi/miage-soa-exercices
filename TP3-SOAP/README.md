# SOAP WebServices


## Prerequisites
-	JDK
-	Netbeans IDE
-	Apache Tomcat or Glassphish
-	SoapUI

## Create a simple SOAP WebService with JAX-WS

Create and deploy a simple calculator web service that take two numbers **(IntA, IntB)** as argument and return the result. The calculator webservice server should expose these methods:
-	Add : return the sum of IntA and IntB;
-	Divid: return the division of **IntA** and **IntB**
-	Multiply: return the multiplication of **IntA** and **intB**
-	Substruct : return the substruction of **IntA** and **IntB**

### Questions
- 1/ Follow the **HowTo simple JAX-WS** steps described below to create the calculator webservice server.
- 2/ Test the calculator web service using the SoapUI application.
- 3/ write a simple Java client that consume the different Calculator web services.

### HowTo simple JAX-WS
- 1/ Open the Netbeans IDE and create a web application named CalculatorWS
- 2/ Add a new web service named  CalculatorService (package : com.miage.calculatorws)
```
Select project > Right-click > New > Web service
```
The generated class **CalculatorService.java** will be stored inside the source packages under the com.miage.calculatorws package. By default Netbeans generate a new webservice with a simple method named “hello”. The hello method takes a string argument as name and return a hello message concatenated with the name passed on argument.
Eg: if name = “Toto”   hello method returns ”hello Toto !”.

- 3/ Clean and build project
```
Select CalculatorWS project > Right-click > clean and build
```
- 4/ Deploy
```
Select CalculatorWS project > Right-click > clean and build
```
- 5/	Check the CalculatorService Descriptor at :
```
http://127.0.0.1:8084/CalculatorWS/CalculatorService
```

- 6/ Test the CalculatorService within the SoapUI test application.
  - Open SoapUI application and create a New SOAP project
  - Copy/past the full path of CalculatorService WSDL generated on (step 5)
  ![pic1](https://i.postimg.cc/9fT8HgvV/pic1.png)
  - To test the CalculatorService and it’s hello method
    - a)	Double click on Request1 to edit the hello request
    - b)	Entre a new value within the name attribute eg: <name> MIAGE </name>
    - c) Send the request by clicking on the Play button
    - d) CalculatorService server must respond with a helloResponse containing the hello message  <return>Hello MIAGE !</return>
    ![pic2](https://i.postimg.cc/7hPm7HbY/pic2.png)
- 7/	Create a simple Client
  - Create a new Java application named CalculatorClient
  -	Connect the CalculatorClient  with the CalculatorService using the wsdl generated at step 5
    -	Right-Click on the CalculatorClient project and add new Web service client
    -	Specify the WSDL file of the Web Service CalculatorService
    ![pic3](https://i.postimg.cc/rmNfK0VP/pic3.png)
    - Click finish : Netbeans will generate new files and folders according to the attached WSDL file.
  - Import hello method within the CalculatorClient.java
  - Locate the hello method on the project tree (as show on the picture bellow)
  ![pic4](https://i.postimg.cc/D0C5P9bQ/pic4.png)
 	- Drag and drop the hello method within the ClaculatorClient class
    ![pic5](https://i.postimg.cc/J7KT8kdy/pic5.png)
  - Add the main java code in order to test hello method
  ```
  eq: System.out.println(hello(“MiageClient”));
  ```
